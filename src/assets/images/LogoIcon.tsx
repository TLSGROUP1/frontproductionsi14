import * as React from 'react';

interface LogoIconProps {
    className?: string;
}

export const LogoIcon: React.FC<LogoIconProps> = (props: LogoIconProps) => (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 51" width="120" height="51" fill="none" className={props.className}>
        <defs>
            <path d="M301.357 87.166H528.384V155.095H301.357z"></path>
        </defs>
        <path
            fill="var(--primary-text-color)"
            strokeWidth="1.579"
            d="M52.221 20.844V9.06l-1.733 1.427-1.234-1.612c.171-.143.4-.336.713-.585l.828-.764.891-.756.714-.577h2.082v14.65h-2.26z"
            className="cls-1"
        ></path>
        <path
            fill="var(--primary-text-color)"
            strokeWidth="1.579"
            d="M68.563 18.654v2.19h-2.297v-2.19H60.01v-1.87l.664-1.426c.256-.549.527-1.177.827-1.826.3-.649.606-1.326.913-2.018.307-.692.62-1.37.913-2.026.293-.656.578-1.284.82-1.854.243-.571.464-1.049.65-1.427H67.2l-4.658 10.428h3.724v-4.28h2.297v4.28h1.234v2.019h-1.234z"
            className="cls-1"
        ></path>
        <path
            fill="var(--primary-text-color)"
            strokeWidth="1.579"
            d="M26.393 35.024q0 5.577-3.402 8.773-3.402 3.195-9.63 3.203a15.514 15.514 0 01-8.16-1.955 14.33 14.33 0 01-5.05-5.028l5.521-3.995a11.27 11.27 0 003.232 3.374 8.003 8.003 0 004.579 1.263c2.14 0 3.666-.521 4.543-1.555a5.75 5.75 0 001.32-3.86 4.993 4.993 0 00-.528-2.403 5.407 5.407 0 00-1.462-1.74 9.159 9.159 0 00-2.29-1.263c-.906-.35-1.883-.713-2.938-1.006a32.419 32.419 0 01-4.993-1.99 12.547 12.547 0 01-3.374-2.446 8.21 8.21 0 01-1.905-3.167 13.132 13.132 0 01-.585-4.095 12.24 12.24 0 01.878-4.672 10.428 10.428 0 012.525-3.701A11.348 11.348 0 018.575 6.35a14.266 14.266 0 015.079-.856 15.692 15.692 0 017.425 1.526 12.083 12.083 0 014.551 4.18l-5.107 4.28a8.859 8.859 0 00-3.053-2.76 8.467 8.467 0 00-3.93-.878 5.428 5.428 0 00-4.052 1.426 4.993 4.993 0 00-1.426 3.61 3.566 3.566 0 00.649 2.168 6.07 6.07 0 001.762 1.62 13.552 13.552 0 002.653 1.233c1.02.371 2.09.713 3.231 1.084a22.176 22.176 0 014.786 2.026 11.598 11.598 0 013.082 2.596 9.116 9.116 0 011.669 3.296 15.435 15.435 0 01.5 4.123z"
            className="cls-1"
        ></path>
        <path
            fill="var(--primary-text-color)"
            strokeWidth="1.579"
            d="M32.856 46.65V20.844h6.42V46.65h-6.42z"
            className="cls-1"
        ></path>
        <path
            fill="var(--primary-text-color)"
            d="M31.514632939773037 9.323972268910438L36.079644384221865 13.161442639478992 40.65178980066048 9.323972268910438 36.079644384221865 5.493642763058233z"
            className="cls-1"
        ></path>
        <path
            fill="var(--primary-text-color)"
            strokeWidth="1.579"
            d="M99.398 31.956h7.133v2.854h-4.38v2.353h3.852v2.854h-3.852v2.418h4.537v2.853h-7.276l-.014-13.332z"
            className="cls-1"
        ></path>
        <path
            fill="var(--primary-text-color)"
            strokeWidth="1.579"
            d="M116.53 38.504l3.995 6.805h-3.224l-2.568-4.48-2.532 4.48h-3.217l3.988-6.805-3.802-6.548h3.224l2.425 4.28 2.332-4.28h3.217l-3.837 6.548z"
            className="cls-1"
        ></path>
        <path
            fill="var(--primary-text-color)"
            strokeWidth="1.579"
            d="M132.06 44.011a5.228 5.228 0 01-3.903 1.54c-3.716 0-6.284-2.745-6.284-6.904 0-3.83 2.14-6.905 6.27-6.905a5.079 5.079 0 013.66 1.391l-1.213 2.29a4.009 4.009 0 00-2.447-.82c-1.854 0-3.36 1.426-3.36 4.044 0 2.368 1.477 4.044 3.631 4.044a4.137 4.137 0 002.604-.97l1.041 2.29z"
            className="cls-1"
        ></path>
        <path
            fill="var(--primary-text-color)"
            strokeWidth="1.579"
            d="M142.33 39.931h-4.365v5.378h-2.739V31.956h2.74v5.115h4.364v-5.115h2.74V45.31h-2.74v-5.378z"
            className="cls-1"
        ></path>
        <path
            fill="var(--primary-text-color)"
            strokeWidth="1.579"
            d="M152.88 31.956h2.446l4.993 13.353h-2.924l-1.034-2.803h-4.744l-.963 2.803h-2.753l4.979-13.353zm-.207 7.69h2.69l-1.342-3.78-1.348 3.78z"
            className="cls-1"
        ></path>
        <path
            fill="var(--primary-text-color)"
            strokeWidth="1.579"
            d="M171.382 45.31l-5.706-8.011v8.01h-2.568V31.956h2.19l5.706 8.053v-8.053h2.568V45.31h-2.19z"
            className="cls-1"
        ></path>
        <path
            fill="var(--primary-text-color)"
            strokeWidth="1.579"
            d="M182.153 37.698h5.121v5.956a6.512 6.512 0 01-4.486 1.883c-4.109 0-6.42-2.746-6.42-6.904 0-3.83 2.311-6.905 6.077-6.905a5.535 5.535 0 013.924 1.391l-1.149 2.461a3.467 3.467 0 00-2.468-.992c-1.969 0-3.495 1.299-3.495 4.045 0 2.46 1.427 4.044 3.566 4.044a3.21 3.21 0 001.884-.663v-1.427h-2.554v-2.889z"
            className="cls-1"
        ></path>
        <path
            fill="var(--primary-text-color)"
            strokeWidth="1.579"
            d="M190.955 31.956h7.133v2.854h-4.366v2.353h3.852v2.854h-3.852v2.418h4.537v2.853h-7.304V31.956z"
            className="cls-1"
        ></path>
        <text
            x="-23.814"
            y="-406.467"
            fill="var(--primary-text-color)"
            strokeWidth="1.491"
            fontFamily="sans-serif"
            fontSize="55.663"
            fontStyle="normal"
            fontWeight="normal"
            transform="translate(107.032 207.8) scale(.45172)"
            xmlSpace="preserve"
        >
            <tspan
                x="-23.814"
                y="-406.467"
                fill="var(--primary-text-color)"
                strokeWidth="1.491"
                fontFamily="Arial"
                fontSize="55.663"
                fontStyle="normal"
                fontWeight="bold"
            >
                CRYPTO
            </tspan>
        </text>
    </svg>
);
