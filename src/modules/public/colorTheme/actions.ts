import {
    CHANGE_COLOR_THEME,
    TOGGLE_MARKET_SELECTOR,
} from './constants';

export interface ChangeColorThemeAction {
    type: string;
    payload: string;
}

export interface ToggleMarketSelectorAction {
    type: string;
}

export type UIActions =
    | ChangeColorThemeAction
    | ToggleMarketSelectorAction;

export const changeColorTheme = (payload: string): ChangeColorThemeAction => ({
    type: CHANGE_COLOR_THEME,
    payload,
});

export const toggleMarketSelector = (): ToggleMarketSelectorAction => ({
    type: TOGGLE_MARKET_SELECTOR,
});
