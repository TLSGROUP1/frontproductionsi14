import classnames from 'classnames';
import { History } from 'history';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect, MapDispatchToPropsFunction } from 'react-redux';
import { Link, RouteProps, withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { LogoutIcon } from '../../assets/images/header/LogoutIcon';
import { ProfileIcon } from '../../assets/images/header/ProfileIcon';
import { SidebarIcons } from '../../assets/images/header/SidebarIcons';
import { pgRoutes } from '../../constants';
import {
  changeUserDataFetch,
  logoutFetch, Market,
  RootState, selectCurrentColorTheme,
  selectCurrentMarket,
  selectUserInfo,
  selectUserLoggedIn, User
} from '../../modules';

interface DispatchProps {
  logoutFetch: typeof logoutFetch;
}

interface ReduxProps {
  colorTheme: string;
  isLoggedIn: boolean;
  currentMarket: Market | undefined;
  user: User;
}

interface OwnProps {
  history: History;
  changeUserDataFetch: typeof changeUserDataFetch;
}

type Props = OwnProps & ReduxProps & RouteProps & DispatchProps;

class BaseHeaderSubnav extends React.Component<Props> {
  public render() {
    const { isLoggedIn } = this.props;

    return (
      <div className="pg-header-subnav">
        {this.renderProfileLink()}
        {pgRoutes(isLoggedIn).map(this.renderNavItem, this)}
        {this.renderLogout()}
      </div>
    );
  }

  private renderProfileLink() {
    const { isLoggedIn, location } = this.props;
    const address = location ? location.pathname : '';
    const isActive = address === '/profile';

    const iconClassName = classnames('pg-header-wrapper-nav-item-img', {
      'pg-header-wrapper-nav-item-img--active': isActive,
    });

    return isLoggedIn && (
      <div className="pg-header-subnav-nav-item">
        <Link to="/profile" className={isActive ? 'route-selected' : undefined}>
          <div className="pg-header-subnav-nav-item-link">
            <div className="pg-header-subnav-nav-item-link-img-wrapper">
              <ProfileIcon className={iconClassName} />
            </div>
            <p className="pg-header-subnav-nav-item-link-text">
              <FormattedMessage id={'page.header.navbar.profile'} />
            </p>
          </div>
        </Link>
      </div>
    );
  }

  private renderNavItem([name, url, img]: string[], index: number) {
    const address = this.props.history.location ? this.props.history.location.pathname : '';
    const { currentMarket } = this.props;
    const path = url.includes('/trading') && currentMarket ? `/trading/${currentMarket.id}` : url;
    const isActive = (url === '/trading/' && address.includes('/trading')) || address === url;

    const iconClassName = classnames('pg-header-subnav-nav-item-img', {
      'pg-header-subnav-nav-item-img--active': isActive,
    });

    return (
      <div className="pg-header-subnav-nav-item" key={index}>
        <Link to={path} className={isActive ? 'route-selected' : undefined}>
          <div className="pg-header-subnav-nav-item-link">
            <div className="pg-header-subnav-nav-item-link-img-wrapper">
              <SidebarIcons className={iconClassName} name={img} />
            </div>
            <p className="pg-header-subnav-nav-item-link-text">
              <FormattedMessage id={name} />
            </p>
          </div>
        </Link>
      </div>
    );
  }

  private renderLogout() {
    const { isLoggedIn } = this.props;

    return isLoggedIn && (
      <div className="pg-header-subnav-nav-item">
        <button onClick={this.props.logoutFetch}>
          <div className="pg-header-subnav-nav-item-link">
            <div className="pg-header-subnav-nav-item-link-img-wrapper">
              <LogoutIcon className="pg-header-subnav-nav-item-img" />
            </div>
            <p className="pg-header-subnav-nav-item-link-text">
              <FormattedMessage id={'page.body.profile.content.action.logout'} />
            </p>
          </div>
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state: RootState): ReduxProps => ({
  colorTheme: selectCurrentColorTheme(state),
  isLoggedIn: selectUserLoggedIn(state),
  currentMarket: selectCurrentMarket(state),
  user: selectUserInfo(state),
});

const mapDispatchToProps: MapDispatchToPropsFunction<DispatchProps, {}> = dispatch => ({
  logoutFetch: () => dispatch(logoutFetch()),
});

export const HeaderSubnav = compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(BaseHeaderSubnav) as React.ComponentClass;
