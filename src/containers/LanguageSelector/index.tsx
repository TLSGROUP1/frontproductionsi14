import * as React from 'react';

import { connect, MapDispatchToPropsFunction } from 'react-redux';
import {
  changeLanguage,
  changeUserDataFetch,
  RootState,
  selectCurrentLanguage,
  selectUserInfo,
  selectUserLoggedIn,
  User
} from '../../modules';

import classnames from 'classnames';
import { Dropdown } from 'react-bootstrap';
import { languages } from '../../api/config';

interface State {
  isOpenLanguage: boolean;
}

interface ReduxProps {
  isLoggedIn: boolean;
  lang: string;
  user: User;
}

interface DispatchProps {
  changeLanguage: typeof changeLanguage;
  changeUserDataFetch: typeof changeUserDataFetch;
}

type Props = ReduxProps & DispatchProps;

class BaseLanguageSelector extends React.Component<Props, State> {
  public state = {
    isOpenLanguage: false,
  };

  public render() {
    const { isOpenLanguage } = this.state;
    const { lang } = this.props;

    const languageName = lang.toUpperCase();

    const languageClassName = classnames('dropdown-menu-language-field', {
      'dropdown-menu-language-field-active': isOpenLanguage,
    });

    return (
      <div className="pg-language-selector">
          <Dropdown>
              <Dropdown.Toggle variant="primary" id={languageClassName}>
                  <img
                      src={this.tryRequire(lang) && require(`../../assets/images/header/${lang}.svg`)}
                      alt={`${lang}-flag-icon`}
                  />
                  <span className="pg-language-selector__language pg-language-selector__language--selected">{languageName}</span>
              </Dropdown.Toggle>
              <Dropdown.Menu>
                  {this.getLanguageDropdownItems()}
              </Dropdown.Menu>
          </Dropdown>
      </div>
    );
  }

  private tryRequire = (name: string) => {
    try {
        require(`../../assets/images/header/${name}.svg`);

        return true;
    } catch (err) {
        return false;
    }
  };

  private getLanguageDropdownItems = () => {
    return languages.map((l: string, index: number) =>
        <Dropdown.Item key={index} onClick={this.handleChangeLanguage.bind(this, l)}>
            <div className="dropdown-row">
                <img
                    src={this.tryRequire(l) && require(`../../assets/images/header/${l}.svg`)}
                    alt={`${l}-flag-icon`}
                />
                <span>{l.toUpperCase()}</span>
            </div>
        </Dropdown.Item>,
    );
  };

  private handleChangeLanguage(language: string) {
    const { user, isLoggedIn } = this.props;

    if (isLoggedIn) {
        const data = user.data && JSON.parse(user.data);

        if (data && data.language && data.language !== language) {
            const payload = {
                ...user,
                data: JSON.stringify({
                    ...data,
                    language,
                }),
            };

            this.props.changeUserDataFetch({ user: payload });
        }
    }

    this.props.changeLanguage(language);
  }
}

const mapStateToProps = (state: RootState): ReduxProps => ({
  isLoggedIn: selectUserLoggedIn(state),
  lang: selectCurrentLanguage(state),
  user: selectUserInfo(state),
});

const mapDispatchToProps: MapDispatchToPropsFunction<DispatchProps, {}> = dispatch => ({
  changeLanguage: payload => dispatch(changeLanguage(payload)),
  changeUserDataFetch: payload => dispatch(changeUserDataFetch(payload)),
});

export const LanguageSelector = connect(mapStateToProps, mapDispatchToProps)(BaseLanguageSelector);
